import os
import sys

import tftpy

server_address = ('localhost', 23000)


def start():
    c = tftpy.TftpClient(
        server_address[0],
        server_address[1],
        {},  # defaults are just fine
        "localhost",
    )

    while True:
        try:
            print("1) list")
            print("2) get file")
            print("3) push file")
            print("4) exit")

            cho = input("\nchoice: ")
            if cho == "1":
                c.download("@@file_list.txt", sys.stdout.buffer)
            elif cho == "2":
                """download specific file"""
                file_name = input("file name: ")
                c.download(file_name, file_name)
                print(f"file {file_name} downloaded in {os.curdir}/{file_name}\n")
            elif cho == "3":
                """upload specific file"""
                file_name = input("file name: ")
                c.upload(file_name, file_name)
                print(f"file {file_name} uploaded!")
            elif cho == "4":
                print("bye :)")
                sys.exit(0)
            else:
                print("invalid command")
        except tftpy.TftpFileNotFoundError:
            print("file not found :/")
        except tftpy.TftpException as err:
            print(str(err))
            sys.exit(1)
        except KeyboardInterrupt:
            sys.exit(0)


if __name__ == "__main__":
    print(f"starting in ")
    start()
