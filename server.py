import io
import sys
from os import listdir
from os.path import isfile, join

import tftpy

base_dir = "data"
server_address = ('0.0.0.0', 23000)


def file_not_found(file, raddress, rport):
    if file != "@@file_list.txt":
        return None
    print(f"serving file list to {raddress}:{rport}")
    return io.StringIO('\n'.join(f"\t> {f}" for f in listdir(base_dir) if isfile(join(base_dir, f))) + "\n")


def start():
    server = tftpy.TftpServer(base_dir, file_not_found)
    print(f"serving on {server_address[0]}:{server_address[1]}")
    try:
        server.listen(server_address[0], server_address[1])
    except tftpy.TftpException as err:
        print(f"error: {str(err)}")
        sys.exit(1)
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    start()
