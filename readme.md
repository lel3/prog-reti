# Relazione Progetto 2022

## Traccia 2

- 921093 - Manuele Pola, manuele.pola@studio.unibo.it

Scambio file con socket UDP

## Introduzione

Questo progetto consiste in due parti: server e client.

Il sistema consente un semplice scambio di files tra server e client utilizzando UDP
come protocollo a livello di trasporto.

Il protocollo più idoneo per raggiungere lo scopo è TFTP, che, a differenza di FTP, utilizza UDP.
Su questo, è stata aggiunta la possibilità di listare i files disponibili sul server.

## Setup

Prima di eseguire il progetto, è opportuno assicurarsi che le librerie necessarie
siano installate.

Eseguire `pip3 install -r requirements.txt` nella root del progetto.

Il progetto di default utilizza la porta 23000. Nel caso si volesse cambiare, occorre
modificarla sia in `server.py`, sia in `client.py`

## Server

Per avviare il server: `python3 server.py`

Il server di default serve i files di esempio presenti nella cartella `data`.

Threads:

![Alt text](./doc/server_threads.svg)

## Client

Per avviare il client: `python3 client.py`
Tutte le operazioni effettuate dal client sono sincrone, quindi eseguite sul thread principale, essendo
impossibile eseguire più operazioni contemporaneamente.

## Operazioni disponibili:

Dal client è possibile:

- listare i files serviti dal server;
- scaricare un file;
- caricare un file.

Una volta avviato il client, è possibile selezionare l'operazione da eseguire inserendo il numero
relativo e premendo invio.
Se si vuole caricare o scaricare un file, è necessario poi inserirene il nome, quando richiesto.
I files scaricati vengono piazzati di default nella stessa cartella in cui il client viene eseguito.